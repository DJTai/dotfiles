#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Functions

#PS1='[\u@\h \W]\$ '

# For signing git commits
export GPG_TTY=$(tty)

# \e  Escape char
# \h  Hostname up to first '.'
# \H  Hostname
# \n  Newline
# \r  Carriage return
# \s  Name of the shell
# \u  Username of current user
# \w  Current working dir with $HOME abbreviated w/ a ~
# \W  Basename of $PWD w/ $HOME abbreviated w/ a ~
# \$  EUID == 0 ? # : $
# \[  Begin a seq of non-printing chars
# \]  End a seq of non-printing chars
# \e[ Color prompt

# Colors used
# 0;32m

colors() {
	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}

__git_ps1() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# Change the window title of X terminals
case ${TERM} in
	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
		;;
	screen*)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
		;;
esac

use_color=true

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true


if ${use_color} ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval $(dircolors -b ~/.dir_colors)
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval $(dircolors -b /etc/DIR_COLORS)
		fi
	fi

	if [[ ${EUID} == 0 ]] ; then
		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
	else
        PS1='\e[1;34m\]'              # change to blue
        PS1=$PS1'\u @ '               # user @
        PS1=$PS1'\[\e[1;35m\]'        # change to purple
        PS1=$PS1'\w'                  # $PWD
        PS1=$PS1'\[\e[3;33m\]'        # change to italics & yellow
        PS1=$PS1'$(__git_ps1)'        # git stuff
        PS1=$PS1'\n\[\e[1;34m\]'      # new line & change to blue
        PS1=$PS1'└─'                  # fancy
        # PS1=$PS1' \$ ▶\[\e[0m\] '     # $ >
        PS1=$PS1' \$\[\e[0m\] '     # $ >
	fi

	alias ls='ls --color=auto'
	alias grep='grep --colour=auto'
	alias egrep='egrep --colour=auto'
	alias fgrep='fgrep --colour=auto'
else
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi

# unset use_color safe_term match_lhs sh
unset use_color match_lhs sh

alias cp="cp -i"      # confirm before overwriting something
alias df='df -h'      # human-readable sizes
alias free='free -m'  # show sizes in MB
alias more=less

complete -cf sudo

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

shopt -s expand_aliases

# export QT_SELECT=4

# Enable history appending instead of overwriting.  #139609
shopt -s histappend

#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

###-----------------------###
###------- My edit -------###
###-----------------------###
update_branch() {
    git checkout $1
    git fatp
    git pull
}

alias eskedit='sudo pacman -Syu'
alias lapback='sudo ~/bin/lapback.sh'

alias ll='ls -lh'

alias vpnc='sudo protonvpn connect'
alias vpnf='sudo protonvpn c -f'
alias vpnd='sudo protonvpn d'
alias vpnr='sudo protonvpn c -r'
alias vpns='sudo protonvpn s'

alias vpn_sec='sudo protonvpn c --sc'
alias vpn_can='sudo protonvpn c --cc ca'
alias vpn_mex='sudo protonvpn c --cc mx'
alias vpn_tor='sudo protonvpn c --tor'

alias dgl='cd ~/dev/gl'
alias dgh='cd ~/dev/gh'
alias devch='cd ~/dev/gh/chamoru'
alias ce-uci='cd ~/dev/gl/ce-uci'
alias up-main='update_branch main'

alias sodda='''
d=$PWD
cd ~/dev/gh/chamoru/chamoru_dictionary
python -u access_dict.py 1
cd $d
'''
alias nayi='''
d=$PWD
cd ~/dev/gh/chamoru/chamoru_dictionary
python -u access_dict.py 2
cd $d
'''

###---------###
###  PYENV  ###
###---------###

eval "$(pyenv init -)"
alias config='/usr/bin/git --git-dir=/home/dabit/.cfg/ --work-tree=/home/dabit'
